name := "crypto-advisor"

version := "1.0"

scalaVersion:= "2.12.4"

assemblyJarName in assembly := "crypto-advisor.jar"
assemblyOutputPath in assembly := new File(s"target/crypto-advisor-${version.value}.jar")

lazy val akkaVersion = "2.5.9"
lazy val akkaHttpVersion = "10.0.11"

libraryDependencies ++= Seq(
	"com.typesafe.akka" % "akka-actor_2.12" % akkaVersion withSources() withJavadoc(),
	"com.typesafe.akka" % "akka-testkit_2.12" % akkaVersion % Test withSources() withJavadoc(),
	"com.typesafe.akka" % "akka-stream_2.12" % akkaVersion withSources() withJavadoc(),
	"com.typesafe.akka" % "akka-http_2.12" % akkaHttpVersion withSources() withJavadoc(),
	"com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion withSources() withJavadoc(),
	"com.typesafe.akka" % "akka-slf4j_2.12" % akkaVersion withSources() withJavadoc(),
	"ch.qos.logback" % "logback-classic" % "1.2.3",
	"commons-codec" % "commons-codec" % "1.9", 
	"org.scalatest" %% "scalatest" % "3.0.4" % "test"
)
