# Crypto Advisor

Platform to help ourselves trade more efficiently by using useful stats, and macros.

## Getting Started

This is an sbt project [sbt](https://www.scala-lang.org/documentation/getting-started-sbt-track/getting-started-with-scala-and-sbt-on-the-command-line.html)

### Eclipse Import

To import it we will be using [sbteclipse](https://github.com/typesafehub/sbteclipse)
In sbt use the command eclipse to create Eclipse project files
> eclipse

After any library change you should do from terminal
> sbt update
> sbt eclipse

### Run

- Run as a scala app from Eclipse
- Run from terminal:
> sbt run

### Deploy

Using [sbt-assembly](https://github.com/sbt/sbt-assembly) 
- Run from terminal (as administrator):
> sbt assembly

We assemble our fat jar and then deploy as a classic java app. 
- Run from terminal:
> java -Xmx512m -jar target/crypto-advisor-1.0.jar

## Endpoints
Akka Http Server Port: 8080

### Binance

#### Generic
- Get Markets "/binance/markets"
- Get Trades "/binance/{symbol}/trades" (i.e. "/binance/ADABTC/trades")
- Get Depths "/binance/{symbol}/depths" (i.e. "/binance/ADABTC/depths")
- Get Klines "/binance/{symbol}/klines?interval=${interval}" (i.e. "/binance/ADABTC/klines?interval=5m")
Kline/Candlestick chart intervals:
	m -> minutes; h -> hours; d -> days; w -> weeks; M -> months
	1m, 3m, 5m, 15m, 30m, 1h, 2h, 4h, 6h, 8h, 12h, 1d, 3d, 1w, 1M

#### Account
- Get Account "/binance/account/{username}/info" (i.e. "/binance/account/petros/info")
application.conf should contain 	
> binance {
	accounts = [
		{
			name = "petros"
			apiKey = "XXX"
			apiSecret = "YYY"
		}
	]
}	
