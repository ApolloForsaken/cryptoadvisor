import org.scalatest.FlatSpec
import org.scalatest.Matchers
import javax.crypto.spec.SecretKeySpec
import javax.crypto.Mac
import java.util.Base64
import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceAccountActor

class SignatureSpect extends FlatSpec with Matchers {
  
  "Signature" should "fucking work" in {
    val apiKey = "vmPUZE6mv9SD5VNHk4HlWFsOr6aKE2zvsw0MuIgwCIPy6utIco14y7Ju91duEh8A" 
    val apiSecret = "NhqPtmdSJYdKjVHjA7PZj4Mge3R5YNiP1e3UZjInClVN65XAbvqqM6A7H5fATj0j"
    
    val queryString = "symbol=LTCBTC&side=BUY&type=LIMIT&timeInForce=GTC&quantity=1&price=0.1&recvWindow=5000&timestamp=1499827319559"
    
    val signature = BinanceAccountActor.getSignature(apiSecret, queryString)
    
    signature should be ("c8db56825ae71d6d79447849e617115f4a920fa2acdcab2b053c4b2838bd6b71")
  }
  
}
