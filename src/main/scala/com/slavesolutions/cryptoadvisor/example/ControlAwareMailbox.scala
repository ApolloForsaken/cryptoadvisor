package com.slavesolutions.cryptoadvisor.example

import akka.dispatch.ControlMessage
import akka.actor.{ Props, Actor, ActorSystem }

// Run using the example.conf (-Dconfig.resoure=/example.conf) (as VM argument)

class Logger extends Actor {
  def receive = {
    case MyControlMessage => println("Oh, I have to process Control message first")
    case x                => println(x.toString)
  }
}

object ControlAwareMailbox extends App {
  val actorSystem = ActorSystem("HelloAkka")
  val actor = actorSystem.actorOf(Props[Logger].withDispatcher("control-aware-dispatcher"))
  actor ! "hello"
  actor ! "how are"
  actor ! "you?"
  actor ! MyControlMessage
} 

case object MyControlMessage extends ControlMessage
