package com.slavesolutions.cryptoadvisor

import akka.actor.ActorSystem
import com.slavesolutions.cryptoadvisor.actor.SupervisorActor

object CryptoAdvisorApp extends App {
  val system = ActorSystem("CryptoAdvisor")
  
  val supervisor = system.actorOf(SupervisorActor.props, "supervisor")

} 
