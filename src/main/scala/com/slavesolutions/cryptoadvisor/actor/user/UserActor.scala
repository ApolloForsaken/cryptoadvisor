package com.slavesolutions.cryptoadvisor.actor.user

import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorLogging
import com.slavesolutions.cryptoadvisor.actor.CryptoActor

class UserActor extends CryptoActor {

	val BINANCE_API_KEY = getConfig.getString("binance.key")
	val BINANCE_API_SECRET = getConfig.getString("binance.secret")
  
  override def preStart(): Unit = {
    log.info("Starting");
  }

  def receive: Actor.Receive = {
    case _ @ value => log.info("Received {}", value)
  }
}

object UserActor {

  def props = Props(new UserActor)

}
