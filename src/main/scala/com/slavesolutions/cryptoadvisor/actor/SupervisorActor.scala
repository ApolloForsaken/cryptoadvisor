package com.slavesolutions.cryptoadvisor.actor

import com.slavesolutions.cryptoadvisor.Settings
import com.slavesolutions.cryptoadvisor.actor.exchange.ExchangesActor

import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.Props
import scala.concurrent.Future
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.model.HttpResponse
import scala.util.Failure
import scala.util.Success
import akka.stream.ActorMaterializerSettings
import akka.stream.ActorMaterializer
import com.slavesolutions.cryptoadvisor.actor.user.UserActor

class SupervisorActor extends CryptoActor {

  override def preStart() = {
    log.info("Starting");
    val exchanges = context.actorOf(ExchangesActor.props, "exchange")
    val user = context.actorOf(UserActor.props, "user")
    val controller = context.actorOf(ControllerActor.props, "controller")
 
  }
  override def receive = {
    case _ @ value => log.info("Received {}", value)
  }
}

object SupervisorActor {

  def props = Props(new SupervisorActor)

}
