package com.slavesolutions.cryptoadvisor.actor

import akka.actor.Props
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.ContentTypes
import akka.http.scaladsl.Http
import akka.actor.ActorRef
import akka.pattern.{ ask, pipe }
import scala.util.Success
import scala.util.Failure
import scala.concurrent.Await
import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceJsonSupport
import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceActor._
import akka.http.scaladsl.server.PathMatcher

class ControllerActor extends CryptoActor {
  
	import com.slavesolutions.cryptoadvisor.actor.ControllerActor._
  import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceActor.GetMarkets
  import com.slavesolutions.cryptoadvisor.actor.exchange.MarketActor._
  import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceAccountActor._
	import BinanceJsonSupport._
  
  val binanceActor = context.actorSelection("/user/supervisor/exchange/binance")
  var binanceAccountActors : Map[String, ActorRef] = Map()
  var binanceMarketActors : Map[String, ActorRef] = Map()
  
  override def preStart(): Unit = {
    log.info("Starting")

    Thread.sleep(10000)
    self ! Init
    
  }

  def receive: Actor.Receive = {
    
    case Init => {
      
      ask(binanceActor, GetAccounts).mapTo[Map[String, ActorRef]].map(binanceAccountActors = _)
      
      ask(binanceActor, GetMarkets).mapTo[Map[String, ActorRef]].map(markets => {
        binanceMarketActors = markets
        log.info("Initialized")
      })
      
      val bindingFuture = Http().bindAndHandle(route, getConfig.getString("server.host"), getConfig.getInt("server.port"))
      
      scala.sys.addShutdownHook {
    	  println("Terminating...")
    	  bindingFuture
    	  .flatMap(_.unbind())
    	  .onComplete { _ =>
    	  m.shutdown()
    	  system.terminate()
    	  }
      }
    }
    
    case _@ value => log.info("Received {}", value)
  }
  
  val route = {
    path("hello") {
      get {
        complete(HttpEntity(ContentTypes.`text/html(UTF-8)`, "<h1>Say hello to Crypto-Advisor</h1>"))
      }
    } ~
    path("binance" / "markets") {
      get {
        complete { binanceMarketActors.keys }
      }
    } ~
    path("binance" / Segment / "depth") {
    	market => 
    	get {
    		complete { binanceMarketActors.get(market).get.ask(GetDepth).mapTo[MarketDepth].map(q => q) }
    	}
    }
    path("binance" / Segment / "trades") {
      market => 
      get {
        complete { binanceMarketActors.get(market).get.ask(GetTrades).mapTo[List[MarketTrade]].map(q => q) }
      }
    } ~
    path("binance" / Segment / "klines" ) { 
      market => 
        parameters('interval) {
          interval =>
          get {
            complete { binanceMarketActors.get(market).get.ask(GetKlines(interval)).mapTo[List[MarketKline]].map(q => q) }
          }
      }
    }
    path("binance" / "account" / Segment / "info") {
      name =>
        get {
          complete { binanceAccountActors.get(name).get.ask(GetAccountInfo).mapTo[AccountInfo].map(q => q) }
        }
    }
  }
  
}

object ControllerActor {

  sealed trait ControllerMessage
  object Init extends ControllerMessage
  
  def props = Props(new ControllerActor)

}
