package com.slavesolutions.cryptoadvisor.actor.exchange

import akka.actor.Props
import com.slavesolutions.cryptoadvisor.actor.CryptoActor

/**
 * Actor supervising all exchange actors (i.e. Binance)
 */
class ExchangesActor() extends CryptoActor {

  override def preStart(): Unit = {
    log.info("Starting");
    val binanceActor = context.actorOf(BinanceActor.props, "binance")
  }

  override def receive = {
    case _ @ value => log.info("Received {}", value)
  }
}

object ExchangesActor {

  def props = Props(new ExchangesActor)

}
