package com.slavesolutions.cryptoadvisor.actor.exchange

import com.slavesolutions.cryptoadvisor.actor.CryptoActor
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec
import java.util.Base64
import scala.concurrent.Future
import akka.pattern.{ ask, pipe }
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.headers
import akka.http.scaladsl.model.headers.ModeledCustomHeader
import akka.http.scaladsl.model.headers.ModeledCustomHeaderCompanion
import scala.util.Try
import akka.http.scaladsl.model.headers.RawHeader
import akka.actor.Props
import akka.http.scaladsl.model.Uri.Query
import akka.http.scaladsl.model.Uri
import java.time.Instant
import scala.util.Success
import scala.util.Failure
import akka.http.scaladsl.model.HttpResponse
import akka.http.scaladsl.model.StatusCodes

/**
 * Actor holding a Binance account 
 */
class BinanceAccountActor(val name: String, val apiKey: String, val apiSecret: String) extends CryptoActor {
  
  import BinanceActor._
  import BinanceAccountActor._
  import BinanceJsonSupport._
  
  override def preStart(): Unit = {
    log.info("Starting");
    
    
  }
  
  def receive = {
    case GetAccountInfo => getInfo pipeTo sender
  }
  
  private val apiHeader = RawHeader(HEADER_API_KEY, apiKey)
  
  private def getSignature(totalParams: String) = BinanceAccountActor.getSignature(apiSecret, totalParams)
  
  private def getInfo: Future[AccountInfo] = {

    val time = getTime.toString
    val totalParams = "timestamp=" + time
    val signature = getSignature(totalParams)
    val queryString = totalParams + "&" + "signature=" + signature;

    Http(context.system)
      .singleRequest(HttpRequest(uri = Uri(ACCOUNT_INFO).withQuery(Query(queryString)), headers = List(apiHeader)))
      .flatMap {
        case HttpResponse(StatusCodes.OK, _, entity, _) => {
          Unmarshal(entity).to[AccountInfo]
        }
        case HttpResponse(status, _, entity, _) => {
          log.error("To Bulo {}", status)
          Unmarshal(entity).to[AccountInfo]
        }
      }
  }
}

object BinanceAccountActor {
  
  import org.apache.commons.codec.binary.Hex
  
  def props(name: String, apiKey: String, apiSecret: String) = Props(new BinanceAccountActor(name, apiKey, apiSecret))
  
  sealed trait BinanceAccountMessage
  object GetAccountInfo extends BinanceAccountMessage
  
  val HEADER_API_KEY = "X-MBX-APIKEY"
 
  def getSignature(apiSecret: String, totalParams: String): String = {
    val sha256_HMAC = Mac.getInstance("HmacSHA256")
    val secret_key = new SecretKeySpec(apiSecret.getBytes("UTF-8"), "HmacSHA256");
    sha256_HMAC.init(secret_key);
    Hex.encodeHexString(sha256_HMAC.doFinal(totalParams.getBytes("UTF-8")));
  }
  
}
