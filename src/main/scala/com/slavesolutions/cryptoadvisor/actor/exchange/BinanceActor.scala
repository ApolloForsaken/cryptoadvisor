package com.slavesolutions.cryptoadvisor.actor.exchange

import akka.actor.Props
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpMethods
import akka.http.scaladsl.model.FormData
import akka.http.scaladsl.model.HttpCharsets
import spray.json.DefaultJsonProtocol
import akka.stream.ActorMaterializer
import akka.stream.ActorMaterializerSettings
import spray.json.RootJsonFormat
import scala.reflect.ClassTag
import spray.json._
import akka.http.scaladsl.server.Directives
import scala.util.{Success, Failure}
import akka.http.scaladsl.unmarshalling.Unmarshal
import scala.concurrent.Future
import akka.http.scaladsl.unmarshalling.Unmarshaller
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.util.Timeout
import scala.concurrent.duration._
import com.slavesolutions.cryptoadvisor.EnumJsonConverter
import com.slavesolutions.cryptoadvisor.actor.CryptoActor
import com.slavesolutions.cryptoadvisor.PriceQtyConverter
import java.time.Instant
import akka.actor.ActorRef
import com.slavesolutions.cryptoadvisor.BinanceMarketKlineConverter

/**
 * Actor for Binance Exchange 
 * <a href='https://github.com/binance-exchange/binance-official-api-docs/blob/master/rest-api.md'>Binance API</a>
 */
class BinanceActor extends CryptoActor {
  
  import CryptoActor._
	import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceActor._
	import scala.concurrent.ExecutionContext.Implicits.global
	import BinanceJsonSupport._

	val ENABLED = getConfig.getBoolean("binance.enabled")
	var markets: Map[String, ActorRef]= Map()
	var accounts: Map[String, ActorRef]= Map()
	
	// Add Behaviors
	var exchangeInfo: ExchangeInfo = null
	
  override def preStart(): Unit = {
    log.info("Starting");
    self ! Init
  }

  override def receive = {
    
    case Init => {
      
      ENABLED match {
        case true =>      
          getConfig.getConfigList("binance.accounts").forEach(account => {
            val name = account.getString("name")
            val apiKey = account.getString("apiKey")
            val apiSecret = account.getString("apiSecret")
            
            accounts += name -> context.actorOf(BinanceAccountActor.props(name, apiKey, apiSecret), name)
          })
          
          log.debug("Binance Accounts: {}", accounts) 
          
          getInfo.onComplete{
            case Success(suc) => {
              createMarketActors(suc.symbols)
              exchangeInfo = suc
              log.info("Initialized")
            }
            case Failure(err) => {
              log.error("To Bulo {}", err.getMessage)
              err.printStackTrace()
            }
      }
        case false => {
          log.warning("Binance is disabled")
          context.stop(self)
        }
      }
 
    }
    
    case GetMarkets => {
      log.debug("Returning markets" + markets)
      sender ! markets
    }
    
    case GetAccounts => {
      log.debug("Returning accounts" + accounts)
      sender ! accounts
    }
    
    case _ @ value => log.info("Received {}", value)
  }
  
  private def getInfo: Future[BinanceActor.ExchangeInfo] = {
    log.debug("Hitting {}", EXCHANGE_INFO)
    Http(context.system).singleRequest(HttpRequest(uri = EXCHANGE_INFO))
      .flatMap(r => Unmarshal(r.entity).to[ExchangeInfo])
//     Http(context.system).singleRequest(HttpRequest(uri = EXCHANGE_INFO))
//     .flatMap{httpResponse => Unmarshal(httpResponse.entity).to[String].map(jsonString => jsonString.parseJson.convertTo[ExchangeInfo])}

  }
  
  private def createMarketActors(symbols: List[Symbol]) = {
    markets = symbols.filter(_.status.equals(TradingStatus.TRADING))
      .map(s => s.symbol -> context.actorOf(MarketActor.props(s.symbol, s.baseAsset, s.quoteAsset), s.baseAsset + "-" + s.quoteAsset))
      .toMap
        
     log.info("Got Markets")
  }
  
}

object BinanceActor {

  sealed trait BinanceMessage
  object GetMarkets extends BinanceMessage
  object GetAccounts extends BinanceMessage
  
  import com.slavesolutions.cryptoadvisor.actor.exchange.RateLimitType._
  import com.slavesolutions.cryptoadvisor.actor.exchange.TradingStatus._

  // Endpoints
  val API_ENDPOINT = "https://api.binance.com/"
  
  // Generic
  val EXCHANGE_INFO = API_ENDPOINT + "api/v1/exchangeInfo"
  val MARKET_DEPTH = API_ENDPOINT + "api/v1/depth"
  val MARKET_TRADES = API_ENDPOINT + "api/v1/trades" 
  val MARKET_KLINES = API_ENDPOINT + "api/v1/klines"
  
  // Account
  val ACCOUNT_INFO = API_ENDPOINT + "api/v3/account"
  
  // Resources
  // Exchange Info
  final case class RateLimit(rateLimitType: RateLimitType, interval: String)
  final case class Symbol(symbol: String, status: TradingStatus, baseAsset: String, quoteAsset: String)
  final case class ExchangeInfo(timezone: String, serverTime: Long, symbols: List[Symbol])
  
  // Market - Depth - Trade - Kline
  final case class PriceQty(price: Double, qty: Double)
  final case class MarketDepth(bids: List[PriceQty], asks: List[PriceQty])
  // TODO: create custom converter or implicit for qty, price to Doubles (or not)
  final case class MarketTrade(id: Long, price: String, qty: String, time: Long, isBuyerMaker: Boolean, isBestMatch: Boolean)
  final case class MarketKline(openTime: Long, open: String, high: String, low: String, close: String, closeTime: Long, volume: String, tradesNo: Int)

  // Account
  final case class AccountInfo(makerCommission: Int, takerCommission: Int, buyerCommission: Int, sellerCommission: Int, 
                               canTrade: Boolean, canWithdraw: Boolean, canDeposit: Boolean, updateTime: Long,
                               balances: List[Balance])
  
  final case class Balance(asset: String, free: String, locked: String)

  // Utility
  def props = Props(new BinanceActor)

}

object RateLimitType extends Enumeration {
  type RateLimitType = Value
  val REQUESTS, ORDERS = Value
}

object TradingStatus extends Enumeration {
  type TradingStatus = Value
  val TRADING = Value
}

object BinanceJsonSupport extends SprayJsonSupport with DefaultJsonProtocol  {
  import BinanceActor._
  import spray.json._
  
  // Exchange Info
  implicit val rateLimitTypeJsonFormat = new EnumJsonConverter(RateLimitType)
  implicit val tradingStatusJsonFormat = new EnumJsonConverter(TradingStatus)

  implicit val symbolJsonFormat: RootJsonFormat[Symbol] = jsonFormat4(Symbol)
  implicit val exchangeInfoJsonFormat: RootJsonFormat[ExchangeInfo] = jsonFormat3(ExchangeInfo)
  
  // Market
  implicit val priceQtyFormat = PriceQtyConverter
  implicit val marketDepthFormat: RootJsonFormat[MarketDepth] = jsonFormat2(MarketDepth)
  implicit val marketTradeFormat: RootJsonFormat[MarketTrade] = jsonFormat6(MarketTrade)
  implicit val marketKlineFormat: RootJsonFormat[MarketKline] = BinanceMarketKlineConverter
  implicit val balanceFormat:     RootJsonFormat[Balance]     = jsonFormat3(Balance)
  implicit val accountInfoFormat: RootJsonFormat[AccountInfo] = jsonFormat9(AccountInfo)
}
