package com.slavesolutions.cryptoadvisor.actor.exchange

import com.slavesolutions.cryptoadvisor.actor.CryptoActor
import akka.actor.Props
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.Http
import scala.util.{Success, Failure}
import akka.http.scaladsl.model.Uri
import akka.http.scaladsl.model.Uri.Query
import spray.json.JsString
import akka.http.scaladsl.model.HttpEntity
import akka.http.scaladsl.model.ContentTypes
import akka.pattern.{ ask, pipe }
import scala.concurrent.duration._

/**
 * Actor for a specific market in a specific exchange (i.e. Binance - ADA/BTC) 
 */
class MarketActor(val symbol: String, val baseAsset: String, val quoteAsset: String) extends CryptoActor {
  
  import com.slavesolutions.cryptoadvisor.actor.CryptoActor._
  import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceActor._
  import com.slavesolutions.cryptoadvisor.actor.exchange.MarketActor._
	import scala.concurrent.ExecutionContext.Implicits.global
	import BinanceJsonSupport._
  
  override def preStart(): Unit = {
    log.debug("Starting {}/{}", symbol, baseAsset, quoteAsset);
  }

  override def receive = {
    case Init => {}
    
    case GetTrades => getTrades pipeTo sender
    
    case GetDepth => getDepth pipeTo sender
    
    case GetKlines(interval) => getKlines(interval) pipeTo sender
    
    case _ @ value => log.info("Received {}", value)
  }
  
  private def getDepth = {
    Http(context.system)
      .singleRequest(HttpRequest(uri = Uri(MARKET_DEPTH).withQuery(Query.apply("symbol" -> symbol))))
      .flatMap(r => Unmarshal(r.entity).to[MarketDepth])
  }
  
  private def getTrades = {
    Http(context.system)
      .singleRequest(HttpRequest(uri = Uri(MARKET_TRADES).withQuery(Query.apply("symbol" -> symbol))))
      .flatMap(r => Unmarshal(r.entity).to[List[MarketTrade]])
  }

  private def getKlines(interval: String) = {
    Http(context.system)
      .singleRequest(HttpRequest(uri = Uri(MARKET_KLINES).withQuery(Query.apply("symbol" -> symbol, "interval" -> interval))))
      .flatMap(r => Unmarshal(r.entity).to[List[MarketKline]])
  }
    
}

object MarketActor {
  
  sealed trait MarketMessage
  object GetDepth extends MarketMessage
  object GetTrades extends MarketMessage
  case class GetKlines(val interval: String) extends MarketMessage

  def props(symbol: String, baseAsset: String, quoteAsset: String) = Props(new MarketActor(symbol, baseAsset, quoteAsset))
  
}



