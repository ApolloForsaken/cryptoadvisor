package com.slavesolutions.cryptoadvisor.actor

import com.slavesolutions.cryptoadvisor.Settings
import akka.actor.ActorLogging
import akka.actor.Actor
import akka.http.scaladsl.server.Directives
import akka.stream.ActorMaterializerSettings
import akka.stream.ActorMaterializer
import akka.util.Timeout
import scala.concurrent.duration._
import java.time.Instant


/**
 * Abstract class for CryptoAdvisor Actors containing any 
 */
abstract class CryptoActor extends Actor with ActorLogging with Directives {

	implicit val system = context.system
  implicit val executionContext = context.system.dispatcher
	implicit val m: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))
	implicit val timeout = Timeout(5 seconds)
	
	def getConfig = context.system.settings.config
	def getTime = Instant.now().toEpochMilli()
	
}

object CryptoActor {
  sealed trait ActorMessage
  object Init extends ActorMessage
}
