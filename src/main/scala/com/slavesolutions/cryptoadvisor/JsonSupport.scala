package com.slavesolutions.cryptoadvisor

import java.util.UUID

import spray.json.{DefaultJsonProtocol, RootJsonFormat}
import com.slavesolutions.cryptoadvisor.actor.exchange.BinanceActor._
import spray.json.JsValue
import spray.json.JsString
import spray.json.DeserializationException
import spray.json.JsArray
import spray.json.JsNumber
import spray.json.JsValue

/**
  * Json Marshalling/Unmarshalling with spray-json library
  * @see <a href= 'http://doc.akka.io/docs/akka-http/10.0.8/scala/http/common/json-support.html'/>
  */
trait JsonSupport extends DefaultJsonProtocol {

  import DefaultJsonProtocol._

}

/**
 * @see <a href= 'https://github.com/spray/spray-json/issues/200'/>
 * <br/>
 * Based on the code found: <a href= https://groups.google.com/forum/#!topic/spray-user/RkIwRIXzDDc'/>
 */
class EnumJsonConverter[T <: scala.Enumeration](enu: T) extends RootJsonFormat[T#Value] {
  override def write(obj: T#Value): JsValue = JsString(obj.toString)

  override def read(json: JsValue): T#Value = {
    json match {
      case JsString(txt) => enu.withName(txt)
      case somethingElse => throw DeserializationException(s"Expected a value from enum $enu instead of $somethingElse")
    }
  }
}

object PriceQtyConverter extends RootJsonFormat[PriceQty] {
  override def write(obj: PriceQty): JsValue = JsString(obj.toString)

  override def read(json: JsValue): PriceQty = {
    json match {
      case JsArray(jsArray) => PriceQty(price = jsArray.head.asInstanceOf[JsString].value.toDouble, qty = jsArray.drop(1).head.asInstanceOf[JsString].value.toDouble)
      case somethingElse => throw DeserializationException(s"Expected a value of PriceQty instead got $somethingElse")
    }
  }
}

object BinanceMarketKlineConverter extends RootJsonFormat[MarketKline] with DefaultJsonProtocol  {

  override def write(obj: MarketKline): JsValue = jsonFormat8(MarketKline).write(obj)

  override def read(json: JsValue): MarketKline = {
    json match {
      case JsArray(jsArray) => MarketKline(openTime = jsArray(0).asInstanceOf[JsNumber].value.toLong, 
          open = jsArray(1).asInstanceOf[JsString].value, 
          high = jsArray(2).asInstanceOf[JsString].value, 
          low = jsArray(3).asInstanceOf[JsString].value, 
          close = jsArray(4).asInstanceOf[JsString].value, 
          volume = jsArray(5).asInstanceOf[JsString].value, 
          closeTime = jsArray(6).asInstanceOf[JsNumber].value.toLong, 
          tradesNo = jsArray(8).asInstanceOf[JsNumber].value.toInt)
      case somethingElse => throw DeserializationException(s"Expected a value of PriceQty instead got $somethingElse")
    }
  }
}

