include "akka-http-version"

server {
	host = 0.0.0.0
	port = 8080
}

binance {
	enabled = true
	
	accounts = [
	]
	
}

akka {

  # Log level used by the configured loggers (see "loggers") as soon
  # as they have been started; before that, see "stdout-loglevel"
  # Options: OFF, ERROR, WARNING, INFO, DEBUG
  loggers = ["akka.event.slf4j.Slf4jLogger"]
  loglevel = "INFO"
  logging-filter = "akka.event.slf4j.Slf4jLoggingFilter"

}

akka.http {

  client {
    # The default value of the `User-Agent` header to produce if no
    # explicit `User-Agent`-header was included in a request.
    # If this value is the empty string and no header was included in
    # the request, no `User-Agent` header will be rendered at all.
    user-agent-header = akka-http/${akka.http.version}

    # The time period within which the TCP connecting process must be completed.
    connecting-timeout = 10s

    # The time after which an idle connection will be automatically closed.
    # Set to `infinite` to completely disable idle timeouts.
    idle-timeout = 60 s

    # The initial size of the buffer to render the request headers in.
    # Can be used for fine-tuning request rendering performance but probably
    # doesn't have to be fiddled with in most applications.
    request-header-size-hint = 512

    # Socket options to set for the listening socket. If a setting is left
    # undefined, it will use whatever the default on the system is.
    socket-options {
      so-receive-buffer-size = undefined
      so-send-buffer-size = undefined
      so-reuse-address = undefined
      so-traffic-class = undefined
      tcp-keep-alive = undefined
      tcp-oob-inline = undefined
      tcp-no-delay = undefined
    }

    # Modify to tweak parsing settings on the client-side only.
    parsing {
      # no overrides by default, see `akka.http.parsing` for default values
    }

    # Enables/disables the logging of unencrypted HTTP traffic to and from the HTTP
    # client for debugging reasons.
    #
    # Note: Use with care. Logging of unencrypted data traffic may expose secret data.
    #
    # Incoming and outgoing traffic will be logged in hexdump format. To enable logging,
    # specify the number of bytes to log per chunk of data (the actual chunking depends
    # on implementation details and networking conditions and should be treated as
    # arbitrary).
    #
    # For logging on the server side, see akka.http.server.log-unencrypted-network-bytes.
    #
    # `off` : no log messages are produced
    # Int   : determines how many bytes should be logged per data chunk
    log-unencrypted-network-bytes = off
  }

  host-connection-pool {
    # The maximum number of parallel connections that a connection pool to a
    # single host endpoint is allowed to establish. Must be greater than zero.
    max-connections = 32

    # The minimum number of parallel connections that a pool should keep alive ("hot").
    # If the number of connections is falling below the given threshold, new ones are being spawned.
    # You can use this setting to build a hot pool of "always on" connections.
    # Default is 0, meaning there might be no active connection at given moment.
    # Keep in mind that `min-connections` should be smaller than `max-connections` or equal
    min-connections = 4

    # The maximum number of times failed requests are attempted again,
    # (if the request can be safely retried) before giving up and returning an error.
    # Set to zero to completely disable request retries.
    max-retries = 3

    # The maximum number of open requests accepted into the pool across all
    # materializations of any of its client flows.
    # Protects against (accidentally) overloading a single pool with too many client flow materializations.
    # Note that with N concurrent materializations the max number of open request in the pool
    # will never exceed N * max-connections * pipelining-limit.
    # Must be a power of 2 and > 0!
    max-open-requests = 512

    # The maximum number of requests that are dispatched to the target host in
    # batch-mode across a single connection (HTTP pipelining).
    # A setting of 1 disables HTTP pipelining, since only one request per
    # connection can be "in flight" at any time.
    # Set to higher values to enable HTTP pipelining.
    # This value must be > 0.
    # (Note that, independently of this setting, pipelining will never be done
    # on a connection that still has a non-idempotent request in flight.
    #
    # Before increasing this value, make sure you understand the effects of head-of-line blocking.
    # Using a connection pool, a request may be issued on a connection where a previous
    # long-running request hasn't finished yet. The response to the pipelined requests may then be stuck
    # behind the response of the long-running previous requests on the server. This may introduce an
    # unwanted "coupling" of run time between otherwise unrelated requests.
    #
    # See http://tools.ietf.org/html/rfc7230#section-6.3.2 for more info.)
    pipelining-limit = 1

    # The time after which an idle connection pool (without pending requests)
    # will automatically terminate itself. Set to `infinite` to completely disable idle timeouts.
    idle-timeout = 30 s

    # Modify to tweak client settings for host connection pools only.
    #
    # IMPORTANT:
    # Please note that this section mirrors `akka.http.client` however is used only for pool-based APIs,
    # such as `Http().superPool` or `Http().singleRequest`.
    client = {
      # The default value of the `User-Agent` header to produce if no
      # explicit `User-Agent`-header was included in a request.
      # If this value is the empty string and no header was included in
      # the request, no `User-Agent` header will be rendered at all.
      user-agent-header = akka-http/${akka.http.version}

      # The time period within which the TCP connecting process must be completed.
      connecting-timeout = 10s

      # The time after which an idle connection will be automatically closed.
      # Set to `infinite` to completely disable idle timeouts.
      idle-timeout = 60 s

      # The initial size of the buffer to render the request headers in.
      # Can be used for fine-tuning request rendering performance but probably
      # doesn't have to be fiddled with in most applications.
      #request-header-size-hint = 512

      # The proxy configurations to be used for requests with the specified
      # scheme.
      proxy {
        # Proxy settings for unencrypted HTTP requests
        # Set to 'none' to always connect directly, 'default' to use the system
        # settings as described in http://docs.oracle.com/javase/6/docs/technotes/guides/net/proxies.html
        # or specify the proxy host, port and non proxy hosts as demonstrated
        # in the following example:
        # http {
        #   host = myproxy.com
        #   port = 8080
        #   non-proxy-hosts = ["*.direct-access.net"]
        # }
        http = default

        # Proxy settings for HTTPS requests (currently unsupported)
        https = default
      }

      # Socket options to set for the listening socket. If a setting is left
      # undefined, it will use whatever the default on the system is.
      socket-options {
        so-receive-buffer-size = undefined
        so-send-buffer-size = undefined
        so-reuse-address = undefined
        so-traffic-class = undefined
        tcp-keep-alive = undefined
        tcp-oob-inline = undefined
        tcp-no-delay = undefined
      }

      # IMPORTANT: Please note that this section is replicated in `client` and `server`.
      parsing {
        # no overrides by default, see `akka.http.parsing` for default values
      }
    }
  }

}